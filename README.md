# Profils QGIS pour l'atelier des journées QGIS FR 2022

Cet entrepôt contient des profils QGIS, la logique de déploiement et le support de l'atelier.

## Tester

Typiquement sur Ubuntu, on peut lancer QGIS ainsi :

```bash
PYQGIS_STARTUP=$PWD/profiles/startup.py qgis
```

### Fichiers logs

Le script de démarrage journalise les lancements de QGIS dans : `~/.local/share/QGIS/QGIS3/logs`
